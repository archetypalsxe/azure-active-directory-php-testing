<?php

require_once("settings.php");

$code = $_REQUEST['code'];
$session_state = $_REQUEST['session_state'];

if(empty($code) || empty($session_state)) {
    header("Location: https://login.microsoftonline.com/{$tenantName}/oauth2/authorize?response_type=code&client_id={$clientId}");
    echo "Did not have code or session state<br/><br/>";
    die;
}

echo "Code is !{$code}!<br/><br/>Session state is !{$session_state}!<br/><br/>";

$fields = [];
$fields['grant_type'] = "client_credentials";
#$fields['grant_type'] = "authorization_code";
$fields['code'] = $code;
$fields['client_id'] = "{$clientId}";
$fields['client_secret'] = $clientKey;
$fields['resource'] = "https://graph.windows.net";
#$fields['resource'] = "{$clientId}";

$fields_string = "";
foreach($fields as $key=>$value) {
    $fields_string .= $key.'='.urlencode($value).'&';
}
$fields_string = rtrim($fields_string,'&');

echo "Fields string: {$fields_string}<br/><br/>";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://login.microsoftonline.com/{$tenantName}/oauth2/token");
curl_setopt($curl, CURLOPT_POST, count($fields));
curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$response = json_decode(curl_exec($curl));

if(empty($response->access_token)) {
    echo "Access token not found<br/><br/>";
    var_dump($response);
    die;
}

echo "Access token is: !{$response->access_token}!<br/><br/>";
var_dump($response); echo "<br/><br/>";

$authHeader = 'Authorization: ' . $response->{'token_type'}.' '.$response->{'access_token'};
$headers = [
    $authHeader,
    'Host: graph.windows.net',
    'Accept:application/json;odata=minimalmetadata',
    'Content-Type:application/json;odata=minimalmetadata',
    'Prefer:return-content'
];

echo "Header: ";
var_dump($headers);
echo "<br/><br/>";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://graph.windows.net/{$tenantName}/users?api-version=1.6");
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
$response = json_decode(curl_exec($curl));
var_dump($response);
